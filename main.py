import cv2
import os
import numpy as np
import csv
from sklearn.neural_network import MLPClassifier
import pandas as pd

# sets to disable use of openCL
# to workaround ORB allocation bug
# https://github.com/Itseez/opencv/issues/6081
cv2.ocl.setUseOpenCL(False)

def auto_canny(image, sigma=0.35):
    v = np.median(image)
    #apply automatic Canny edge detection using computed median
    lower = int(max(0,(1.0 - sigma) * v))
    upper = int(min(255,(1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    return edged

def preparetrainingset(hog, imageset, string, types):
    filename = open('./training/training_'+ string + '.csv', 'wb')
    a = csv.writer(filename, delimiter=',')
    for image in imageset:
        pos = np.zeros([1,0], dtype='float32')
        img = cv2.imread(image)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        r_img_gray = cv2.resize(gray, (64,128))
        dhog = np.array(hog.compute(r_img_gray), dtype=np.float32)
        for i in range(dhog.shape[0]):
            pos = np.append(pos, dhog[i][0])
        pos = np.append(pos, int(1)) if (types == 1) else np.append(pos, int(0))
        a.writerow(pos)
    filename.close()

def getFiles(fileDir):
    files = []
    for dirName, subdirList, fileList in os.walk(fileDir):
        for thisfile in fileList:
            string = dirName + "/" + thisfile
            files.append(string)
    return files

def setuptrainingsets():
    print 'Creating training set...'
    positivetrain = getFiles('./positive')
    negativetrain = getFiles('./negative')
    preparetrainingset(hog, positivetrain, 'positive', 1)
    preparetrainingset(hog, negativetrain, 'negative', 0)

def neural_train(model, trainingset, labelset):
    rootDir = './training/'
    for dirName, subdiList, fileList in os.walk(rootDir):
        for fileName in fileList:
            training_data = np.loadtxt(rootDir+fileName, delimiter=',', dtype=np.float32)
            label_temp = np.zeros((0,1), dtype=np.float32)
            for data in training_data:
                label_temp = data[-1]
                label_temp = np.reshape(label_temp, (1,1))
                train_temp =np.array(np.delete(data, -1), dtype=np.float32)
                train_temp = np.reshape(train_temp, (1,train_temp.shape[0]))
                labelset = np.vstack((labelset, label_temp))
                trainingset = np.vstack((trainingset, train_temp))
    return trainingset, labelset

def neural_check(model, test):
    model.load('mlp.xml')

    ret, resp = model.predict(test)
    prediction = resp.argmax(-1)
    true_labels = test_labels.argmax


    mlp.save('mlp/mlp.xml')
    ret, resp = mlp.predict(train)
    prediction = resp.argmax(-1)
    print 'Prediction:', prediction
    true_labels = train_labels.argmax(-1)
    print 'True labels:', true_labels
    print 'Testing...'
    train_rate = np.mean(prediction == true_labels)
    print 'Train rate: %f:' % (train_rate*100)

layers = np.array([3780, 15, 10, 1])
criteria = (cv2.TERM_CRITERIA_COUNT | cv2.TERM_CRITERIA_EPS, 500, 0.0001)
criteria2 = (cv2.TERM_CRITERIA_COUNT, 100, 0.001)
model = cv2.ml.ANN_MLP_create()
model.setLayerSizes(layers)
model.setTrainMethod(cv2.ml.ANN_MLP_BACKPROP)
model.setActivationFunction(cv2.ml.ANN_MLP_SIGMOID_SYM)
# model.setTermCriteria(criteria)
model.setBackpropMomentumScale(0.05)
model.setBackpropWeightScale(0.05)

trainingset = np.zeros((1, 3780), dtype=np.float32)
labelset = np.zeros((1,1), dtype=np.float32)
hog = cv2.HOGDescriptor('hog.xml')

mlp = MLPClassifier(hidden_layer_sizes=(720,10,1), activation='tanh', algorithm='sgd', alpha=0.0001, batch_size='auto', learning_rate='adaptive', power_t=0.5, max_iter=500)

# setuptrainingsets()
# print 'Training...'
trainingset, labelset = neural_train(model, trainingset, labelset)
mlp.fit(trainingset, labelset)
# print 'Training done.'
#
#
# print 'Training...'
# model.train(trainingset, cv2.ml.ROW_SAMPLE, labelset)
# model.save('mlp.xml')

def mMLP(mlp):
    # df = pd.read_csv('trainingsetNaiveBayes.csv', header=None, na_values=['.'])
    test = pd.read_csv('testout.csv', header=None, na_values=['.'])
    targets = test.iloc[:,3780]
    features = test.columns[0:3779]
    for test2 in test:
        print test2
        resp = mlp.predict(test2)
        prediction = resp.argmax(-1)
    # mnb = nb.fit(df[features], targets).predict(test[features])
        test.loc[:,3780] = pd.Series(prediction, index=test.index)
        test.to_csv('testout.csv', header=None, na_values=['.'])

# mMLP(model)
# print labelset
cap = cv2.VideoCapture(0)
while(True):

    # Capture frame-by-frame
    ret, frame = cap.read()
    frame = cv2.resize(frame,(720,480))
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    edges = auto_canny(gray)
    _, contours, hierarchy = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)
        if (w*h > 800):
            pos = np.zeros([1,0], dtype=np.float32)
            # cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
            obj = gray[y:y+h,x:x+w]
            obj = cv2.resize(obj, (64,128))
            cnt_hog = np.array(hog.compute(obj), dtype=np.float32)
            for i in range(cnt_hog.shape[0]):
                pos = np.append(pos, cnt_hog[i][0])
            # prediction = mlp.predict(pos)
            prediction = mlp.predict(pos)
            print prediction
            # print prediction
            # ret, resp = model.predict(pos)
            # prediction = resp.argmax(-1)
            # true_labels = labelset.argmax(-1)
            # print 'Prediction: %s, Label: %s' % (prediction, true_labels)
            # train_rate = np.mean(prediction == true_labels)
            # print 'Train rate: %f:' % (train_rate*100)
            # if prediction == np.float32(1):
            #     cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)

    cv2.imshow('frame', pos)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
